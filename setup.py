from distutils.core import setup
setup(name='paramfield',
      version='0.1.1',
      download_url='https://bitbucket.org/DataGreed/django-paramfield/downloads/django-paramfield_0.1.tar.gz',
      author='Alexey Strelkov',
      author_email='datagreed@gmail.com',
      url='http://bitbucket.org/DataGreed/django-paramfield/',
      packages=['paramfield'],
      license='MIT',
      long_description="ParamField is a reusable Django field for storing key-value data with not unique keys.")