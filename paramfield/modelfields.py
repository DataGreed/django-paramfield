#!/usr/bin/evn python
# -*- coding: UTF8 -*-
#coding=UTF8

"""
@author: Alexey "DataGreed" Strelkov
@since: 15:04:38 26.01.2009
@summary: a key-value field, with non-unique keys for saving arrays of similar params
@requires: Django 1.2+
"""

from django.db import models
from django.utils.encoding import smart_unicode
from django.core import exceptions

class ParamHolder(object):
    u'''
        A local python class for storing params.
            @param params: a dictionary of string params, looks like that: {'a':['1','2'], 'b':['3']}, dict
            @raise ValueError: if passed params is not a dict-based type
            @warning: it's better to use ParamHolder methods to manipulate params instead of manipulating the dictionary itself
    '''
    def __init__(self,params=None):
        u'''
            @param params: a dictionary of string params, looks like that: {'a':['1','2'], 'b':['3']}, dict
            @raise ValueError: if passed params is not a dict-based type
        '''
        
        if isinstance(params, dict) or (params is None):
            self.params = params or {}
        else:
            raise ValueError(u"params must be a dicitonary-based type variable!")
        
    def get_param_names(self):
        u'''
            Returns a list of parameter keys
         '''        
        return self.params.keys()    
        
    def get_param_values(self, param_name):
        u'''
        Returns a list of values for parameter with key param_name
        @param param_name: parameter key, str
        '''       
        return self.params[param_name]
                        
    def has_param(self,param_string):
        u'''
            Сhecks if there is a parameter with given key.
                @param param_string: parameter key, str
                @rtype: bool
        '''
        try:
            self.params[smart_unicode(param_string)]
            return True
        
        except KeyError:
            return False
    
    
            
    def set_param(self,param_name,param_value):
        u'''
            Sets a value for a key
                @param param_name: parameter key
                @param param_value: parameter value, list
                @deprecated: proved to be uncomortable. Maybe will be added again sometime later
        '''
        raise "Deprecated. Use add_param_value to add parameter values"
    
    def param_has_value(self,param_name,param_value):
        u'''
            Checks if parameter with key param_name has a value of param_value
        '''
        try:
            self.params[smart_unicode(param_name)].index(smart_unicode(param_value))
            return True
        
        except ValueError:
            return False
        
    def has_param_value(self,param_name,param_value):
        u'''A synonym for param_has_value()'''
        return self.param_has_value(param_name, param_value)
    
    def add_param_value(self,param_name,param_value):
        u'''
            Adds a value of param_value to parameter with key param_name, if such value does not exist within this parameter.
            If there is no parameter with key param_name, creates one with given value,
        '''
        try:
            #if there is no such value, we'll add one:
            if not self.param_has_value(param_name, param_value):
                self.params[smart_unicode(param_name)].append(smart_unicode(param_value))
            
        except KeyError:
            
            self.params[smart_unicode(param_name)] = [smart_unicode(param_value)]
    
    
    def remove_param_value(self,param_name, param_value):
        u'''
            Removes a value of param_value from values of params[param_name]
            
            Returns True if succeed, False otherwise.
        '''
        try:
            self.params[smart_unicode(param_name)].pop(self.params[smart_unicode(param_name)].index(smart_unicode(param_value)))
            return True
        
        except:
            return False
    
    def remove_param(self,param_name):
        u'''
            Removes a parameter with key param_name (with all it's values!)
                @param param_name: a key for parameter to remove
                @return: False if parametr does not exist, True if succeed
                @rtype: bool 
        '''
        try:
            self.params.pop(smart_unicode(param_name))
            return True
        
        except KeyError:
            return False
        
    
    def serialize(self):
        u'''
            Serializes an object for storing in the database
                @warning: uses ParamField.delimeter and ParamField.param_delimeter as delimeters
        '''
        #TODO: Should delimeters be in this class? Serialization is here only for sake of __unicode__() method (to display value in admin site, etc.)
        
        #initial value:
        serialized_value = u""
                        
        #serializing params:
        for key in self.params:
            if key: #omitting empty keys

                for value in self.params[key]:#going through all param values:
                    
                    #escaping:
                    value = ParamHolder.escape(value)
                    
                    #adding to serialized value string:
                    serialized_value += ParamField.delimeter + ParamHolder.escape(key) + ParamField.param_delimeter + value
        
        try:
            if serialized_value[0]==ParamField.delimeter:
               
                serialized_value=serialized_value[1:serialized_value.__len__()]
     
        except IndexError:
            pass
        
        #adding delimeters to the beginning and end of he string, so you can search like this: ";some:search_term;"
        serialized_value=  u"%s%s%s" % (ParamField.delimeter, serialized_value, ParamField.delimeter)
        
        return smart_unicode(serialized_value)
    
    
    @classmethod
    def escape(cls, value):
        u'''
            Escapes string for serialization
         '''
        
        return (u"%s" % value).replace(ParamField.delimeter, ParamField.delimeter_escaped).replace(ParamField.param_delimeter, ParamField.param_delimeter_escaped)
    
    def __unicode__(self):
        
        return self.serialize()


class ParamField(models.TextField):
    '''
        A class representing a Parameter Field for database storage.
        Stored in the following form:
            "param1:foo;param1:morefoo;someparam:bar;someotherparam:lol"
        Stores non-unique "key:value" data
    '''
    
    __metaclass__ = models.SubfieldBase

    
    #:a delimeter for key:values, unicode object
    delimeter = u";"
    
    #:an escaped delimeter
    delimeter_escaped = u"&semicolon%"
    
    #:an inner delimeter for key:values, unicode object
    param_delimeter = u":"
    
    #:an escaped delimeter
    param_delimeter_escaped = u"&colon%"
    
    
    
    def to_python(self, value):
        u'''
            Deserialization of an object from database to an object of ParamHolder class
        '''

        if value is None:
            if self.null:
                return value
            else:
                raise exceptions.ValidationError(u"This field cannot be empty (null is set to False in field settings - null=False)")
        
        if not isinstance(value, basestring):
            value = smart_unicode(value)
        
        #deserialization:
        try:
            if value[0]== self.delimeter:
                value = value[1:]
        except IndexError:
            pass
        
        try:
            if value[value.__len__()-1]== self.delimeter:
                value = value[:value.__len__()-1]
        except IndexError:
            pass
                    
        
        
        temp_list = value.split(self.delimeter)
        params = {}
        
        
        for item in temp_list:
            temp_item = item.split(self.param_delimeter)
            
            #de-escaping characters:
            for i in range(temp_item.__len__()):
                temp_item[i]=temp_item[i].replace(self.delimeter_escaped, self.delimeter).replace(self.param_delimeter_escaped, self.param_delimeter)
                
            if temp_item.__len__()>1:
                #we've got a parameter
                
                    try:    #if there was a value for this param already, let's add another one:
                        params[temp_item[0]].append(temp_item[1])
                    except KeyError:    #or let's create a new one
                        params[temp_item[0]]=[ temp_item[1] ]
#                params[temp_item[0]]=temp_item[1]
            else:
                #it's not a parameter, it's a tag (an older version of ParamField had that)
                #TODO: handle this situation somehow. Maybe some debug output will be good for that?
                pass
                
        return ParamHolder(tags, params)


    def get_prep_value(self, value):
        u'''
            Serialization of a ParamHolder object to string for storing in a database
            
            @note: in django 1.1 and below - get_db_prep_value
                   in django 1.2 and above - get_prep_value
        '''

        
        if value is None:
            #if None, create an empty ParamHolder, so it wouldn't raise an unnecessary error
            value = ParamHolder()
        
        if not isinstance(value, ParamHolder):
            raise exceptions.ValidationError("Value must be a ParamHolder, but given a %s instead" % type(value))
        
            #FIXME: check that there are no delimeters in keys

        return value.serialize()
    
    def value_to_string(self, obj):
        value = self._get_val_from_obj(obj)

        return self.get_prep_value(value)

